# Versions of rpm and yum for major CentOS releases 7 and back

## Centos 7 (amazon variant example)

rpm is 4.11 with yum 3.4

[root@ip-aa-bb-cc-dd ec2-user]# rpm -q yum
yum-3.4.3-158.amzn2.0.2.noarch
[root@ip-aa-bb-cc-dd ec2-user]# rpm -q rpm
rpm-4.11.3-25.amzn2.0.3.x86_64


## Centos 6 for comparison (historical now)

rpm is 4.8 with yum 3.2

[root@centos6bit64 ~]# rpm -q rpm
rpm-4.8.0-55.el6.x86_64
[root@centos6bit64 ~]# rpm -q yum
yum-3.2.29-81.el6.centos.noarch

