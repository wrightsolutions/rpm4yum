#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) 2018 Gary Wright http://www.wrightsolutions.co.uk/contact
# Distributed under the BSD '3 clause' software license, see the accompanying
# file COPYING or https://opensource.org/licenses/BSD-3-Clause

from os import getenv as osgetenv
from sys import exit,argv
import re
import shlex
from subprocess import Popen,PIPE
#import subprocess

# rpmwhat answer 'what' style questions for rpm/yum based GNU/Linux
# rpmwhat.py binutils
# Above would (assuming binutils installed) show all the reasons NOT to uninstall it
# rpmwhat.py '*bin/false'
# Above would show all packages that provide a filename matching the given glob

# chr(58) is colon (:)	; chr(61) is equals (=)	; chr(41) is closing parenth ())
# chr(35) is hash / octothorpe  ; chr(58) is colon (:)  ;
SPACER=chr(32)
HASHER=chr(35)
COLON=chr(58)
DOTTY=chr(46)
#DOUBLE_DOT=chr(46)*2
PARENTH_OPEN=chr(40)
PARENTH_CLOSE=chr(41)

re_neededby = re.compile('is\s+needed\s+by')
re_neededby_p = re.compile("is\s+needed\s+by\*{0}\w+{1}".format(PARENTH_OPEN,PARENTH_CLOSE))
re_packages_excluded = re.compile('[0-9]+\s+packages\s+excluded')
re_no_matches_found = re.compile('No matches found')
re_repo = re.compile("Repo\s+{0}\s+".format(COLON))
re_matched_from = re.compile("Matched\s+from\s+{0}\s+".format(COLON))
re_filename = re.compile("Filename\s+{0}\s+".format(COLON))
re_name_colon_word = re.compile("[a-zA-z0-9\.\-]+\s+{0}\s+\w+".format(COLON))
re_space_colon_space = re.compile("{0}{1}{0}".format(SPACER,COLON))

BUFSIZE=4096		#BUFSIZE=1024
BINRPM='/bin/rpm'
BINYUM='/bin/yum'
""" Next value MAXDEPTH_THROTTLE should probably be 7 or 8 max as
even with 7 it would take SIX SECONDS to complete the following:
  python2 ./rpmwhat.py rpm
"""
MAXDEPTH_THROTTLE=7

HEADER_PROVIDES_TEMPLATE="""## Similar purpose to: yum whatprovides '{0}'
## ... or '*lib/setup.py'
"""

HEADER_REQUIRES_TEMPLATE="""## Similar output to: rpm -q --whatrequires '{0}'
## Key: 0TLD0 indicates a top level dependency
## and || indicates 'no more depth' so probably end of depends chain.
## Assuming you piped output to /tmp/rpm.txt then following command for uniq list:
## fgrep -v '#' /tmp/rpm.txt | cut -d' ' -f2 - | sort | uniq"""


def before_colon(colon_string):
	before = ''
	try:
		before = colon_string.split(COLON, 1)[0]
	except:
		pass
	if len(before) > 0:
		before = before.strip()
	return before


def after_colon(colon_string):
	after = ''
	try:
		after = colon_string.split(COLON, 1)[1]
	except:
		pass
	if len(after) > 0:
		after = after.lstrip()
	return after


def sub5(sub_command,verbosity=0):
	""" Execute subprocess popen for the given command
	Return a 4 tuple of stdout as iterator, stdout, stderr, and return code
	"""
	cmdlex = shlex.split(sub_command)
	subbed = Popen(cmdlex,bufsize=BUFSIZE,stdout=PIPE,stderr=PIPE)
	subout, suberr = subbed.communicate()
	# .returncode is only populated after a communicate() or poll/wait
	subrc = subbed.returncode
	if verbosity > 1 and subrc > 2:
		print("Error {0} during {1}".format(ncon_rc,cmd_ncon))
	elif verbosity > 2:
		if subrc > 0:
			print("subrc={0} from .returncode={1}".format(subrc,subbed.returncode))
	else:
		pass
	subout_iter = iter(subout.splitlines())
	try:
		suberr_iter = iter(suberr.splitlines())
	except AttributeError:
		suberr_iter = iter([])
	return (subout_iter,subout,suberr,suberr_iter,subrc)


def neededby_list_populate(pname,iter_lines,maxdepth=20,verbosity=0):
	""" From provided lines iterator, populate neededby_list
	First argument pname is really just for debug as not used
	otherwise in logic/processing.
	"""
	neededby_list = []
	if maxdepth < 1:
		if verbosity > 2:
			print("maxdepth={0} means exit neededby_list_populate".format(maxdepth))
		return []
	idx = 1
	while idx <= maxdepth:
		try:
			line = iter_lines.next()
			if re_neededby.search(line):
				if verbosity > 2:
					print('search(line) was positive')
				needed_name = line.split()[-1]
				neededby_list.append(needed_name)
				#print(needed_name)
			if verbosity > 2:
				print("line: {0}".format(line))
		except StopIteration:
			if verbosity > 1:
				print("{0} breaking out when idx={1}".format(pname,idx))
			break
		idx+=1

	return neededby_list


def pinstalled(pname,maxdepth=8,verbosity=0,prefix_num=None):
	"""
	The second argument is your brake on recursion and
	suggest you think carefully before setting maxdepth>8
	as you might wait a while for completion!!

	When first called (top level) suggest omit the final
	argument prefix_num or set it to None
	Next level (first nested level) will set an
	appropriate prefix_num so that display is as intended
	when nesting is in play.
	"""

	if verbosity > 2:
		print("pinstalled({0},... running with maxdepth={1}".format(pname,maxdepth))
	elif prefix_num is None:
		print("# Recursion will be limited by your setting maxdepth={0}".format(maxdepth))
	else:
		pass

	cmd_rpm = "{0} --test -e {1}".format(BINRPM,pname)
	outiter, subout, suberr, erriter, subrc = sub5(cmd_rpm,verbosity)

	if 0 == subrc:
		if verbosity > 1:
			print("Probable no depth as subrc={0} from {1}".format(subrc,pname))
		if prefix_num is not None:
			print("-{0}-|| {1}".format(prefix_num,pname))
		else:
			print("||{1}".format(prefix_num,pname))
	elif 1 == subrc:
		# 1 is typical and means there are some things requiring the package
		neededby_list = neededby_list_populate(pname,erriter,maxdepth,verbosity)
		if verbosity > 1:
			print("subrc={0} and len(neededby_list)={1}".format(subrc,
								len(neededby_list)))
		for nidx,needname in enumerate(neededby_list):
			cmd_rpm = "{0} --test -e {1}".format(BINRPM,needname)
			if prefix_num is not None:
				print("-{0}-> {1}".format((1+nidx),needname))
			else:
				print("0TLD0 {1} 0TLD0".format((1+nidx),needname))
			outiter, subout, suberr, erriter, subrc = sub5(cmd_rpm,verbosity)
			if 1 == subrc:
				nested_list = neededby_list_populate(needname,erriter,
								maxdepth,verbosity)
				for nestedname in nested_list:
					pinstalled(nestedname,(maxdepth-1),
						verbosity,prefix_num=(1+nidx))
					# Above call DID SUPPLY prefix_num final arg
					print("-{0}-> {1}".format((1+nidx),nestedname))
	elif verbosity > 1:
		print("subrc={0} when processing {1}".format(subrc,pname))
	return subrc


def provides(provides_glob,verbosity=0):
	#outiter, subout, suberr, suberr_iter, subrc = sub5(cmd_rpm,verbosity)
	cmd_yum = "{0} whatprovides '{1}'".format(BINYUM,provides_glob)
	outiter, subout, suberr, erriter, subrc = sub5(cmd_yum,verbosity)

	matches_count = None
	blank_lines_count = 0
	result_lines = []

	if 0 == subrc:
		if verbosity > 1:
			print("provides({1},...) rc={2} next for outiter".format('',
								provides_glob,subrc))
		for line in outiter:
			if re_packages_excluded.match(line):
				print("{0}{0} Exclusion Note: {1}".format(HASHER,line))
			elif re_no_matches_found.match(line):
				matches_count = 0
			elif re_space_colon_space.search(line):
				result_lines.append(line)
			else:
				""" We want to keep blank lines because
				they delimit our provides result block(s)
				"""
				try:
					if len(line.strip()) < 1:
						result_lines.append(line)
						blank_lines_count += 1
				except:
					pass

	elif 1 == subrc:
		for line in erriter:
			print(line)
	else:
		for line in erriter:
			print(line)

	if verbosity > 2:
		print("provides({1},...) has {2} blank lines in results".format('',
						provides_glob,blank_lines_count))

	return result_lines


def provides_result_lines_process(res_lines,print_flag=False,verbosity=0):
	slines = []
	provides_dict = {}
	idx = 0
	name_colon_word = 'default'
	repo = 'default'
	filename_list = []
	for line in res_lines:
		if re_matched_from.match(line):
			pass
		elif re_filename.match(line):
			filename_list.append(after_colon(line))
			#print(filename_list)
		elif re_repo.match(line):
			repo = after_colon(line)
			if repo.startswith('installed'):
				repo = repo.upper()
		elif re_name_colon_word.match(line):
			name_colon_word = before_colon(line)
			#print(name_colon_word,name_colon_word)
		elif len(line.strip()) < 2:
			if verbosity > 2:
				print(filename_list)
			if name_colon_word is not None and name_colon_word != 'default':
				key_colon_word = "{0}{1}{2}".format(repo,COLON,name_colon_word)
				provides_dict[key_colon_word] = filename_list
			name_colon_word = None
			repo = 'default'
			filename_list = []
		else:
			pass

	if verbosity > 2:
		print(provides_dict)

	for provider,file_list in sorted(provides_dict.items()):
		sline = "{0} provides {1}".format(provider,file_list)
		slines.append(sline)

	if print_flag is True:
		for line in slines:
			print(line)

	return slines


if __name__ == '__main__':

	program_binary = argv[0].strip()
	from os import path
	program_dirname = path.dirname(program_binary)

	try:
		pacname = argv[1]
	except IndexError:
		pacname = None

	PYVERBOSITY_STRING = osgetenv('PYVERBOSITY')
	PYVERBOSITY = 1
	if PYVERBOSITY_STRING is None:
		pass
	else:
		try:
			PYVERBOSITY = int(PYVERBOSITY_STRING)
		except:
			pass
	#print(PYVERBOSITY)

	""" Next verbosity_global = 1 is the most likely outcome
	unless the user specifically overrides it by setting
	the environment variable PYVERBOSITY
	"""
	if PYVERBOSITY is None:
		verbosity_global = 1
	elif 0 == PYVERBOSITY:
		# 0 from env means 0 at global level
		verbosity_global = 0
	elif PYVERBOSITY > 1:
		# >1 from env means same at global level
		verbosity_global = PYVERBOSITY
	else:
		verbosity_global = 1
    

	qrc = 101
	res_lines = []
	provides_lines = []
	if pacname is None:
		pass
	elif pacname.startswith('*bin/'):
		# Continue as if user is requesting a 'whatprovides' style query
		print(HEADER_PROVIDES_TEMPLATE.format(pacname))
		res_lines = provides(pacname,verbosity_global)
	elif pacname.startswith('*lib/'):
		# Continue as if user is requesting a 'whatprovides' style query
		print(HEADER_PROVIDES_TEMPLATE.format(pacname))
		#print(HEADER_PROVIDES)
		res_lines = provides(pacname,verbosity_global)
	else:
		# Continue as if user is requesting a 'whatrequires' style query of dependencies
		print(HEADER_REQUIRES_TEMPLATE.format(pacname))
		#print(HEADER_REQUIRES)
		qrc = pinstalled(pacname,MAXDEPTH_THROTTLE,verbosity_global)

	if len(res_lines) > 0:
		provides_lines = provides_result_lines_process(res_lines,True,
							verbosity=verbosity_global)
	
	if verbosity_global > 1 and qrc > 2:
		print("Error {0} during querying for {1}".format(qrc,pacname))

	exit(qrc)

